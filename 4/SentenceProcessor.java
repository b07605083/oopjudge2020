public class SentenceProcessor {
    public String removeDuplicatedWords(String sentence){
        String[] words=sentence.split(" ");               //Split the input sentence into words without spaces, and store them in a String type array.
        StringBuilder result= new StringBuilder(words[0]);      //Create a StringBuilder type object as a container to be the result output at the end.
        for(int i=0;i<words.length;i++){
            if(!result.toString().contains(words[i])){          //Append every string in "words" to "result" if "result" doesn't contain the string.
                result.append(" ");                             //Insert a space to separate every word.
                result.append(words[i]);
            }
        }
        return result.toString();
    }

    public String replaceWord(String target, String replacement, String sentence){
        String[] words=sentence.split(" ");       //The first two lines have the same effects in removeDuplicatedWords.
        StringBuilder result=new StringBuilder();
        for(int i=0;i<words.length;i++){
            result.append(" ");                        //Insert a space to separate every word.
            if(words[i].equals(target)){
                result.append(replacement);           //If there are the same strings as "target" in "words[]", then append "replacement" to "result".
            }
            else{
                result.append(words[i]);              //Otherwise, append "words[i]" to "result".
            }
        }
        result.deleteCharAt(0);                       //Since "result" would have an unnecessary space at beginning, just delete it so that I can get the final result.
        return  result.toString();
    }
}
