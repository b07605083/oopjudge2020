public class Triangle extends Shape {

    public Triangle(double length) {                  //Use Shape's constructor to create a Triangle's constructor with length set.
        super(length);
    }

    @Override
    public void setLength(double length) {      //Set length.
        this.length = length;
    }            //Set length.

    @Override
    public double getArea() {                                                //Calculate the area of a triangle.
        return Math.round(Math.sqrt(3)/4*Math.pow(length,2)*100.0)/100.0;
    }

    @Override
    public double getPerimeter() {                                           //Calculate the perimeter of a triangle.
        return Math.round(3*length*100.0)/100.0;
    }
}
