public class ShapeFactory {
    public enum Type {           //Define enum of Type.
        Triangle,
        Square,
        Circle
    }
    public Shape createShape(ShapeFactory.Type shapeType, double length) {       //Create a Shape's object via given shapeType and length.
        switch (shapeType) {
            case Triangle:
                return new Triangle(length);
            case Square:
                return new Square(length);
            case Circle:
                return new Circle(length);
            default:
                return null;
        }
    }
}