public class Square extends Shape {

    public Square(double length) {                  //Use Shape's constructor to create a Square's constructor with length set.
        super(length);
    }

    @Override
    public void setLength(double length) {      //Set length.
        this.length = length;
    }

    @Override
    public double getArea() {                                                //Calculate the area of a square.
        return Math.round(Math.pow(length,2)*100.0)/100.0;
    }

    @Override
    public double getPerimeter() {                                           //Calculate the perimeter of a square.
        return Math.round(4*length*100.0)/100.0;
    }
}
