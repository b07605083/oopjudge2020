public class Circle extends Shape {

    public Circle(double length) {                  //Use Shape's constructor to create a Circle's constructor with length set.
        super(length);
    }

    @Override
    public void setLength(double length) {      //Set length.
        this.length = length;
    }            //Set length.

    @Override
    public double getArea() {                                                //Calculate the area of a circle.
        return Math.round(Math.PI*Math.pow(length/2,2)*100.0)/100.0;
    }

    @Override
    public double getPerimeter() {                                           //Calculate the perimeter of a circle.
        return Math.round(Math.PI*length*100.0)/100.0;
    }
}
