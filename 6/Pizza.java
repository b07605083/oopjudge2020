public class Pizza {
    private String Size=new String();                                //Declare private instance variables.
    private int NumberOfCheese, NumberOfPepperoni, NumberOfHam;
    public Pizza(){                                                  //Default constructor to create a pizza.
        Size="small";
        NumberOfCheese=1;
        NumberOfPepperoni=1;
        NumberOfHam=1;
    }
    public Pizza(String Size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam){    //The constructor to create a pizza according to user's inputs.
        if(Size.equals("small") || Size.equals("medium") || Size.equals("large")){
            this.Size=Size;
        }
        else{                                                                           //All exceptions of each input are handled in this constructor.
            System.out.println("The size has to be either small, medium or large!");
        }
        if(NumberOfCheese>=0 && NumberOfCheese<=100){
            this.NumberOfCheese=NumberOfCheese;
        }
        if(NumberOfCheese<0 || NumberOfCheese>100){
            System.out.println("NumOfCheese has to be in 0~100!");
        }
        if(NumberOfPepperoni>=0 && NumberOfPepperoni<=100){
            this.NumberOfPepperoni=NumberOfPepperoni;
        }
        if(NumberOfPepperoni<0 || NumberOfPepperoni>100) {
            System.out.println("NumOfPepperoni has to be in 0~100!");
        }
        if(NumberOfHam>=0 && NumberOfHam<=100){
            this.NumberOfHam=NumberOfHam;
        }
        if(NumberOfHam<0 || NumberOfHam>100) {
            System.out.println("NumOfHam has to be in 0~100!");
        }
    }

    public String getSize() {                                                          //Size getter.
        return Size;
    }

    public void setSize(String Size) {                                                   //Size setter, exception handling included.
        if(Size.equals("small") || Size.equals("medium") || Size.equals("large")){
            this.Size=Size;
        }
        else{
            System.out.println("The size has to be either small, medium or large!");
        }
    }

    public int getNumberOfCheese() {                            //NumberOfCheese getter
        return NumberOfCheese;
    }

    public void setNumberOfCheese(int NumberOfCheese) {                  //NumberOfCheese setter, exception handling included
        if(NumberOfCheese>=0 && NumberOfCheese<=100){
            this.NumberOfCheese=NumberOfCheese;
        }
        else{
            System.out.println("NumOfCheese has to be in 0~100!");
        }
    }

    public int getNumberOfPepperoni() {                               //NumberOfPepperoni getter
        return NumberOfPepperoni;
    }

    public void setNumberOfPepperoni(int NumberOfPepperoni) {          //NumberOfPepperoni setter, exception handling included
        if(NumberOfPepperoni>=0 && NumberOfPepperoni<=100){
            this.NumberOfPepperoni=NumberOfPepperoni;
        }
        else {
            System.out.println("NumOfPepperoni has to be in 0~100!");
        }
    }

    public int getNumberOfHam() {                                    //NumberOfHam getter
        return NumberOfHam;
    }

    public void setNumberOfHam(int NumberOfHam) {                     //NumberOfHam setter, exception handling included
        if(NumberOfHam>=0 && NumberOfHam<=100){
            this.NumberOfHam=NumberOfHam;
        }
        else {
            System.out.println("NumOfHam has to be in 0~100!");
        }
    }
    public double calcCost(){                                     //Calculate the cost of the pizza.
        double cost;
        switch (Size){
            case "small":
                cost=10.0;
                break;
            case "medium":
                cost=12.0;
                break;
            case "large":
                cost=14.0;
                break;
            default:
                cost=0;
        }
        cost=cost+2.0*(NumberOfCheese+NumberOfPepperoni+NumberOfHam);
        return cost;
    }
    public boolean equals(Pizza pizza){                //Determine whether this pizza is the same as the other one by their size, the number of cheese toppings, the number of pepperoni toppings, and the number of ham toppings.
        if(this.Size==pizza.Size && this.NumberOfCheese==pizza.NumberOfCheese && this.NumberOfPepperoni==pizza.NumberOfPepperoni && this.NumberOfHam==pizza.NumberOfHam){
            return true;
        }
        else{
            return false;
        }
    }
    public String toString(){                          //Return a String containing the pizza size, quantity of each topping.
        return "size = "+Size+", numOfCheese = "+NumberOfCheese+", numOfPepperoni = "+NumberOfPepperoni+", numOfHam = "+NumberOfHam;
    }
}
