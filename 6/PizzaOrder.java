public class PizzaOrder {
    private int numberPizzas;                             //Declare private instance variables.
    private Pizza pizza1, pizza2, pizza3;
    public boolean setNumberPizzas(int numberPizzas){     //Declare the method with int type input "numberPizzas".
        if(numberPizzas>=1 && numberPizzas<=3){           //If the input is within 1~3, then set it to be the instance variable "numberPizzas", and return true.
            this.numberPizzas=numberPizzas;
            return true;
        }
        else {                                             //Otherwise, return false.
            return false;
        }
    }
    public void setPizza1(Pizza pizza1){                //Set every pizza respectively.
        this.pizza1=pizza1;
    }
    public void setPizza2(Pizza pizza2){
        if(numberPizzas>1){
            this.pizza2=pizza2;
        }
        else {                            //The setPizza2 and setPizza3 methods will be used only if there are two or three pizzas in the order, respectively.
            System.out.println("You only order one pizza!");
        }
    }
    public void setPizza3(Pizza pizza3){
        if(numberPizzas>2){
            this.pizza3=pizza3;
        }
        else if(numberPizzas==1){
            System.out.println("You only order one pizza!");
        }
        else {
            System.out.println("You only order two pizza!");
        }
    }
    public double calcTotal(){                                            //Calculates the total cost of the order by using the calcCost() method in the "Pizza" class.
        if(numberPizzas==1){
            return pizza1.calcCost();
        }
        else if(numberPizzas==2){
            return pizza1.calcCost()+pizza2.calcCost();
        }
        else {
            return pizza1.calcCost()+pizza2.calcCost()+pizza3.calcCost();
        }
    }
}
