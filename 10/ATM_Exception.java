public class ATM_Exception extends Exception {
    enum ExceptionTYPE {            //Define an enum ExceptionTYPE containing the two error messages.
        BALANCE_NOT_ENOUGH,
        AMOUNT_INVALID
    }

    private ExceptionTYPE excptionCondition;                //Define a private field representing the exception condition.

    public ATM_Exception(ExceptionTYPE excptionCondition) { //Constructor created by the exception condition.
        this.excptionCondition = excptionCondition;
    }

    public String getMessage() {                    //A method that returns the error message.
        return excptionCondition.toString();
    }
}
