public class Simple_ATM_Service implements ATM_Service {
    @Override
    public boolean checkBalance(Account account, int money) throws ATM_Exception {    //Override the checkBalance method.
        if(account.getBalance() >= money) {
            return true;
        }
        else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);    //If the balance is not enough, throw an ATM_Exception.
        }
    }

    @Override
    public boolean isValidAmount(int money) throws ATM_Exception {               //Override the checkBalance method.
        if(money%1000 == 0) {
            return true;
        }
        else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);    //If the input money is invalid, throw an ATM_Exception.
        }
    }

    @Override
    public void withdraw(Account account, int money) {
        try {                                                 //If checkBalance and isValidAmount both pass, update the balance.
            checkBalance(account, money);
            isValidAmount(money);
            account.setBalance(account.getBalance()-money);
            System.out.println("updated balance : " + account.getBalance());
        } catch (ATM_Exception e) {                            //Otherwise, print the error message and the balance.
            System.out.println(e.getMessage());
            System.out.println("updated balance : " + account.getBalance());
        }
    }
}
