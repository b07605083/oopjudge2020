public class GreenCrud {
    public int calPopulation(int initialSize, int days){  //Declare a public class named "calPopulation" with two int type inputs: "initialSize" and "days" respectively.
        int pop=1, prePop=0, nextPop=1, n, i;       //Declare and initialize variables for computing the Fibonacci sequence.
        if((initialSize<1 || initialSize>10000) || (days<1 || days>100)){           //Handle the exception of accepting invalid inputs.
            System.out.println("Invalid inputs! \"initialSize\" must be within 1~10000 and \"days\" must be within 1~100.");
        }
        else{                             //The main program computing the Fibonacci sequence.
            n=days/5;                       //Determine the halting term of the Fibonacci sequence.
            for(i=1;i<=n;i++){          //Here I use for loop to compute, not recursion. Since recursion is not efficient compared to for loop.
                pop = prePop + nextPop;
                prePop = nextPop;
                nextPop = pop;
            }
            pop*=initialSize;          //Times initialSize to pop.
        }
        return pop;
    }
}
