public class IsLeapYear {
    private int year;                    //Declare a private variable named "year".
    public boolean determine(int year){           //Declare a public class named "determine" with an int type input and a boolean type output.
        this.year=year;                    //Assign the input to the member variable "year".
        if(year<1 || year>10000){           //Handle the exception of accepting an invalid input.
            System.out.println(year+" is an invalid year! Please key in an integer within 1~10000.");
            return false;
        }
        else{                              //The main program to determine whether the valid input year is a leap year or not.
            if(year%400==0)
            {
                return true;
            }
            else if(year%4==0&&year%100!=0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
