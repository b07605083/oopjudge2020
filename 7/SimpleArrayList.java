public class SimpleArrayList {
    private Integer[] array;                  //Declare a private Integer type array field.
    public SimpleArrayList() {                //Default constructor that sets the array size to zero.
        array = new Integer[0];
    }

    public SimpleArrayList(int n) {           //Constructor that can set initial array size, and set all Integer to zero in the array
        array = new Integer[n];
        for(int i=0;i<n;i++) {
            array[i] = 0;
        }
    }

    public void add(Integer i) {
        Integer[] newArray = new Integer[array.length+1];                           //Use a new array to store the original array and i.
        System.arraycopy(array, 0, newArray, 0, array.length);
        newArray[newArray.length-1] = i;
        array = newArray;
    }

    public Integer get(int index) {                 //Get the element of this array by the given index, or return null if the position is out of range, instead.
        if(index>=0 && index<array.length) {
            return array[index];
        }
        else {
            return null;
        }
    }

    public Integer set(int index, Integer element) {  //Replace the element at the specified position in this array with the specified element, and return the original element at that specified position.
        if(index>=0 && index<array.length) {
            Integer origin = array[index];
            array[index] = element;
            return origin;

        }
        else {                    //If the specified position is out of range of the array, return null.
            return null;
        }
    }

    public boolean remove(int index) {
        if(array[index]==null) {           //If a null element is at the specified position, return false.
            return false;
        }
        else {             //Remove the element at the specified position, and shift any subsequent elements to the left if remove successfully.
            Integer[] newArray = new Integer[array.length-1];
            System.arraycopy(array, 0, newArray, 0, index);
            System.arraycopy(array, index+1, newArray, index, newArray.length-index);
            array=newArray;
            return true;
        }
    }

    public void clear() {
        array = new Integer[array.length];
    }   //Remove all of the elements from this array.

    public int size() {
        return array.length;
    }          //Return the number of elements in this array.

    public boolean retainAll(SimpleArrayList l) {
        SimpleArrayList newArrayList = new SimpleArrayList();
        int countNull = 0;                            //A counter to count the quantity of null elements in this array.
        for(int i=0;i<this.size();i++) {
            for(int j=0;j<l.size();j++) {
                if(this.array[i]!=null) {
                    if((this.array[i].equals(l.get(j)))) {
                        newArrayList.add(l.get(j));             //Use a new array list to store the matched elements.
                        break;
                    }
                }
                else {
                    countNull++;
                    break;
                }
            }
        }
        if(this.size()-countNull==newArrayList.size()) {         //Check if this array is the same as itself before except those null elements.
            this.array = newArrayList.array;                     //Assign the new array to this array.
            return false;
        }
        else {
            this.array = newArrayList.array;                     //Assign the new array to this array.
            return true;
        }
    }

}
