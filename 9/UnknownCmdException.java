public class UnknownCmdException extends Exception {
    public UnknownCmdException(String errMessage) {           //Constructor of UnknownCmdException created via the error message.
        super(errMessage);
    }
}