import java.text.DecimalFormat;

public class SimpleCalculator {
    private double result=0.0;                                       //Define private instance variables.
    private int count=0;
    private String cmd = "";                 //Used to store input command, operator and value.
    private String operator;
    private double value;
    public void calResult(String cmd) throws UnknownCmdException {
        if(!endCalc(cmd)) {                    //If the calculation is not end, then split the input cmd and store them in the container array.
            String[] container =  cmd.split(" ");
            if(container.length != 2) {                       //If the length of this array is not equal to 2, throw an UnknownCmdException.
                throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
            }
            else if(isNumber(container[1])) {      //Check whether the input is a number. If yes, parseDouble it and check the input operator.
                value = Double.parseDouble(container[1]);
                switch (container[0]) {       //If the operator and the value are both legal, then do the calculation and update count.
                    case "+":
                        result += value;
                        operator = "+";
                        count++;
                        break;
                    case "-":
                        result -= value;
                        operator = "-";
                        count++;
                        break;
                    case "*":
                        result *= value;
                        operator = "*";
                        count++;
                        break;
                    case "/":
                        if(value == 0) {                                           //If the denominator is 0, throw an UnknownCmdException.
                            throw new UnknownCmdException("Can not divide by 0");
                        }
                        else {
                            result /= value;
                            operator = "/";
                            count++;
                        }
                        break;
                    default:                               //If the operator is illegal, throw an UnknownCmdException.
                        throw new UnknownCmdException(container[0] + " is an unknown operator");
                }
            }
            else {     //If the value isn't a number, throw an UnknownCmdException. The message is determined by the legality of the operator.
                if(container[0].equals("+") || container[0].equals("-") || container[0].equals("*") || container[0].equals("/")) {
                    throw new UnknownCmdException(container[1] + " is an unknown value");
                }
                else {
                    throw new UnknownCmdException(container[0] + " is an unknown operator and " + container[1] + " is an unknown value");
                }
            }
        }
    }

    public String getMsg() {                //Return message according to different kinds of conditions.
        DecimalFormat df = new DecimalFormat("###0.00");
        if(endCalc(cmd)) {
            return "Final result = " + df.format(result);
        }
        else if(count == 0) {
            return "Calculator is on. Result = 0.00";
        }
        else if(count == 1) {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". New result = " + df.format(result);
        }
        else {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". Updated result = " + df.format(result);
        }
    }

    public boolean endCalc(String cmd) {            //Determine whether the calculation is end or not.
        if(cmd.equals("r") || cmd.equals("R")) {
            this.cmd = cmd;
            return true;
        }
        else {
            return false;
        }

    }

    public static boolean isNumber(String s){       //Method that is used to determine whether the input string is a number.
        try{
            Double.parseDouble(s);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}